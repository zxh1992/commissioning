
[bdl_names, pathname] = uigetfile( ...
    {'*.txt','Text-files (*.txt)'; ...
    '*.*',  'All Files (*.*)'}, ...
    'Pick a file', ...
    'MultiSelect', 'on');
if(not(iscell(bdl_names)))
    bdl_names = {bdl_names};
end

for i=1:length(bdl_names)
    
    BDLs{i} = load_BDL(fullfile(pathname,bdl_names{i}));
    
end

figure
subplot(3,3,1)
hold on
title('Mean energy difference (from nominal)')
for i=1:length(bdl_names)
    plot(BDLs{i}.NominalEnergy,BDLs{i}.MeanEnergy-BDLs{i}.NominalEnergy);
end
subplot(3,3,2)
hold on
title('Energy spread')
for i=1:length(bdl_names)
    plot(BDLs{i}.NominalEnergy,BDLs{i}.EnergySpread);
end
subplot(3,3,3)
hold on
title('Protons per MU')
for i=1:length(bdl_names)
    plot(BDLs{i}.NominalEnergy,BDLs{i}.ProtonsMU);
end
subplot(3,3,4)
hold on
title('Spot-size x')
for i=1:length(bdl_names)
    plot(BDLs{i}.NominalEnergy,BDLs{i}.SpotSize1x);
end
subplot(3,3,5)
hold on	
title('Divergence x')
for i=1:length(bdl_names)
    plot(BDLs{i}.NominalEnergy,BDLs{i}.Divergence1x);
end
subplot(3,3,6)
hold on
title('Correlation x')
for i=1:length(bdl_names)
    plot(BDLs{i}.NominalEnergy,BDLs{i}.Correlation1x);
end
subplot(3,3,7)
hold on
title('Spot-size y')
for i=1:length(bdl_names)
    plot(BDLs{i}.NominalEnergy,BDLs{i}.SpotSize1y);
end
subplot(3,3,8)
hold on
title('Divergence y')
for i=1:length(bdl_names)
    plot(BDLs{i}.NominalEnergy,BDLs{i}.Divergence1y);
end
subplot(3,3,9)
hold on
title('Correlation y')
for i=1:length(bdl_names)
    plot(BDLs{i}.NominalEnergy,BDLs{i}.Correlation1y);
end
legend(strrep(bdl_names,'_',' '))
