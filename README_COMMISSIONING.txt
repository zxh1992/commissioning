Prerequisites:
----------------
- This version of the commissioning tool is compatible with Linux and Windows operating system
- A recent version of Matlab is required, including these 3 toolboxes: image processing, optimization, global optimization
- Commissioning measurements data exported from the RayStation or Eclipse


WARNING:
--------
The commissioning tool will repeatedely run many Monte Carlo simulations in order to optimize the beam model. This may take a lot of computation time (up to a few days), depending on the computer resources and the number of energies to optimize. We recommend to run the automatic commissioning process on a high performance computer.


Getting started:
-----------------
Two sample datasets are provided in the "Sample_data/" folder. They contain the commissioning measurements that will be used to generate a beam model for the MCsquare dose engine. CSV format correspond to RayStation files and ASC/TXT format to Eclipse files.
Run the following Matlab script to generate the BDL using the sample data: "Matlab_Scripts/main.m".
It calls four different Matlab functions: one creating the BDL, one tuning the phase space parameters, one tuning the energy spectrum and one tuning the protons/MU.
This may take a few hours of computation, depending on the computer resources.
The BDL file "MCsquare/BDL/BDL_P1.txt" will be updated at each step of the process. The final result should be close to the sample BDL "MCsquare/BDL/Sample_BDL.txt".


Generate a BDL for your facility:
---------------------------------
- The code to be launched is "main.m". 
- The user must only modify the first part of the file "main.m", named "Data - To be modified by user". Different info about measurements process and measurements files must be completed. More info is needed if Eclipse files are input.
- The folder "DataDir" mentioned "in main.m" is the folder containing all measurements files extracted from RayStation (v6 or v8)
- Adjust the size of your ionization chamber if necessary.
- The absolute dose is generally measured in a 10x10 cm2 field. Adjust the spot spacing and/or field size if necessary.
- The CT used for the tuning of energy spectrum and absolute dose must be chosen carefully. It must be deep enough (second component) to allow the simulation of high energy Bragg peaks and large enough (first and third component) to allow the simulation of the field measured for absolute dosimetry. The file provided with this code should do the job. For fields above 12 cm width, however, 'CT_large' should be used.
- The CT file can be dicom or mhd. If mhd, the associated raw file must be in the same folder as the mhd file.
- The file "BP_library_50to280.mat" (provided with this code) needs to be stored in the same folder as the code.
- The code uses a calibration curve called "Water_only" (provided with this code). This must be a folder in the folder "Scanners" of MCsquare. The files inside must convert any HU to water.
- For the commissioning, a double precision version of MCsquare (provided with this code) should be used. In addition, the MCsquare launcher should be named "MCsquare_double". These two files should be placed in the directory "MCsquare".
- The resulting beam model will be created in the "MCsquare/BDL/" folder.
- By using use_gui=1, optimize=0 and rerunning main.m after having computed the BDL, an evaluation of the BDL based on commissioning measurement data can be displayed for the phase space and the energy spectrum parameters. 
