function name = remove_special_chars(name)

name = name(isstrprop(name,'digit')|isstrprop(name,'lower')|isstrprop(name,'upper')|name=='_');