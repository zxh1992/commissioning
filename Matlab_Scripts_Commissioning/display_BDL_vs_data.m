function display_BDL_vs_data(BDLname, MeasFile, nozzle2iso)

%% Phase space

% Read BDL
A = textread(BDLname,'%s','delimiter','\n');
mask_header = find(~cellfun(@isempty,strfind(A(1:length(A)-1),'NominalEnergy')));
BDL_parameters = importdata(BDLname, ' ', mask_header);
energy = BDL_parameters.data(:,1);
spotsize(:,1) = BDL_parameters.data(:,6);
div(:,1) = BDL_parameters.data(:,7);
corr(:,1) = BDL_parameters.data(:,8);
spotsize(:,2) = BDL_parameters.data(:,9);
div(:,2) = BDL_parameters.data(:,10);
corr(:,2) = BDL_parameters.data(:,11);


% Meas file
[mdata,all_energies,all_depths,all_curves,mask_begin,mask_end,nozzle_dist] = read_SpotProfiles(MeasFile)
depths = unique(all_depths);
raystation=1;
if(not(isnan(nozzle_dist))) % RayStation format
    xycurve = {'X', 'Y'};
else % Eclipse format
    xycurve = {'MeasuredSpotFluenceX', 'MeasuredSpotFluenceY'};
    nozzle_dist = nozzle2iso;
    raystation=0;
end


% Fitting - tuning of PS for all commissioning energies
fun = @(x,xdata)x(3).*exp((-1*(xdata-x(1)).^2)./(2*x(2)^2));
opts = optimset('Display','off');

for j = 1:length(energy)
        
    
    clearvars coeff;
    ind1 = find(round(all_energies*10)/10==energy(j));
    
    dsid = [];
    X_tot = [];
    Z_tot = [];
        
    for i = 1:2
        
        ind2 = find(all_curves==xycurve{i});
        
        for k = 1:length(depths)
            
            ind3 = find(all_depths==depths(k));
            ind = intersect(intersect(ind1,ind2),ind3);
            
            if(isempty(ind))
                continue
            end
            
            clearvars Z X info_image resolution x y X ind_max_x ind_max_y f data;

            % Data
            if(raystation==1) % RayStation format
                z = regexprep(mdata(mask_begin(ind)+1:mask_end(ind)-1),'[;,]',' ');
                if(isempty(z))
                    disp('No data found');
                end
                for l=1:size(z,1)
                    data(l,:) = str2double(strsplit(strtrim(z{l}),' '));
                end
                if data(5,i) >= data(1,i)
                    X = data(:,i);
                    zz = data(:,3);
                else
                    X = flip(data(:,i));
                    zz = flip(data(:,3));
                end
            else
                z = regexprep(mdata(mask_begin(ind)+1:mask_end(ind)-1),'[<>]','');
                if(isempty(z))
                    disp('No data found');
                end
                for l=1:size(z,1)
                    data(l,:) = str2double(strsplit(strtrim(z{l}),' '));
                end
                indd = find(abs(data(1,1:3)-data(2,1:3))>1e-4);
                X = data(:,indd(1));
                zz = data(:,4);
            end

            % Normalize
            zz(zz<0) = 0;
            Z = zz./trapz(X,zz);
            [ind_max] = find(Z==max(Z));

            % Gaussian fit
            
            sol = lsqcurvefit(fun,[X(ind_max(1)),5,1],X,Z,[],[],opts);
            sig_meas(j,i,k) = sol(2);
           
            % Computed with BDL and Courant Snyder 
            D = -abs(depths(k)-nozzle_dist(1));
            sig_MC(j,i,k) = sqrt(spotsize(j,i)^2 - 2*corr(j,i)*div(j,i)*spotsize(j,i)*D + div(j,i)^2*D^2);
            
                        
        end
        
    end
end

err_sig = (sig_meas-sig_MC); %./sig_meas*100

for i=1:length(depths)
    h=figure;
    subplot(1,2,1);
    yyaxis left;
    plot(energy, sig_meas(:,1,i), '*', energy, sig_MC(:,1,i), 'o')
    ylabel('Spot size (mm)');
    yyaxis right;
    plot(energy, err_sig(:,1,i), '--')
    xlabel('Energy (MeV)');
    ylabel('Error (mm)');
    title(['Spot sizes agreement in X, distance from iso = ', num2str(depths(i))])
    legend('Measured spot sizes', 'Computed spot sizes', 'Error')
    subplot(1,2,2);
    yyaxis left;
    ylabel('Spot size (mm)');
    plot(energy, sig_meas(:,2,i), '*', energy, sig_MC(:,2,i), 'o')
    yyaxis right;
    plot(energy, err_sig(:,2,i), '--')
    xlabel('Energy (MeV)');
    ylabel('Error (mm)');
    title(['Spot sizes agreement in Y, distance from iso = ', num2str(depths(i))])
    legend('Measured spot sizes', 'Computed spot sizes', 'Error')
    set(gcf, 'Units', 'Inches', 'Position', [0, 0, 18.25, 7.125], 'PaperUnits', 'Inches', 'PaperSize', [18.25, 7.125])
    saveas(h, ['Spotsize_agreement_depth_', num2str(depths(i)), '.png'])
    saveas(h, ['Spotsize_agreement_depth_', num2str(depths(i)), '.fig'])
end

%% Energy spectrum

h=openfig('ES_evaluation.fig', 'new', 'visible');
set(gcf, 'Units', 'Inches', 'Position', [0, 0, 18.25, 7.125], 'PaperUnits', 'Inches', 'PaperSize', [18.25, 7.125]);
saveas(h,'ES_evaluation.png');
saveas(h,'ES_evaluation.fig');
y = load('results_ES.txt');
Errors = table(y(:,1), y(:,2), y(:,3), y(:,4), y(:,5), 'VariableNames',{'Energy','Range_80','Range_20','BP_Width','Dose_To_Peak'});
%Errors = table(y(:,1), y(:,2), y(:,3), y(:,4), y(:,5), 'VariableNames',{'Energy (MeV)','Error in range 80 (mm)','Error in range 20 (mm)','Bragg peak width error (mm)','Dose-to-peak error'});
writetable(Errors);
Errors


end








