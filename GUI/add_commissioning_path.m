function add_commissioning_path()

c_path = fileparts(fileparts(mfilename('fullpath')));

addpath(c_path);
d = dir(c_path);
for i=1:length(d)
    if(d(i).isdir && (not(strcmp(d(i).name(1),'.')))) % do not include hidden folder (e.g. ".git")
        addpath(genpath(fullfile(c_path,d(i).name)));
    end
end