function test_BDL_and_Scanner_on_data(BDL_filename,Scanner_dirname,uncertainty,RT_folder,output_filename,app)

addpath_reggui();

% default inputs
if(nargin<4)
    uncertainty = 5;
elseif(isempty(uncertainty) || not(isnumeric(uncertainty)))
    uncertainty = 5;
end
if(nargin<5)
    output_filename = fullfile(RT_folder,'illustrations');
elseif(isempty(output_filename))
    output_filename = fullfile(RT_folder,'illustrations');
end
if(nargin<6)
    app = [];
end

% initialize
handles = Initialize_reggui_handles([],'dataPath',RT_folder);
handles = Initialize_reggui_workflow(handles);

% find and import RT data
d = dir(fullfile(RT_folder,'*.dcm'));
DoseFilename = '';
if(not(isempty(d)))
    for i=1:length(d)
        filename = fullfile(RT_folder,d(i).name);
        dicom_header = dicominfo(filename);
        if(strcmp(dicom_header.Modality,'CT'))
            CTFilename = d(i).name;
        elseif(strcmp(dicom_header.Modality,'RTPLAN'))
            PlanFilename = d(i).name;
        elseif(strcmp(dicom_header.Modality,'RTSTRUCT'))
            ContourFilename = d(i).name;
        elseif(strcmp(dicom_header.Modality,'RTDOSE'))
            if(not(isfield(dicom_header.ReferencedRTPlanSequence.Item_1,'ReferencedFractionGroupSequence')))
                if(isempty(DoseFilename))
                    DoseFilename = d(i).name;
                elseif(strcmpi(dicom_header.DoseType,'PHYSICAL'))
                    DoseFilename = d(i).name;
                end
            end
        end
    end
end
handles = Import_image(RT_folder,CTFilename,'dcm','CT',handles);
handles = Import_image(RT_folder,DoseFilename,'dcm','TPS_dose',handles);
handles = Import_plan(RT_folder,PlanFilename,'dcm','Plan',handles);

FileNameIN=fullfile(RT_folder,ContourFilename);
[~,ct_info] = Get_reggui_data(handles,'CT');
StructOUT = read_dicomrtstruct(FileNameIN,ct_info);
body_index=[];
fields = fieldnames(StructOUT.DicomHeader.RTROIObservationsSequence);
for i=1:numel(fields)
    if strcmpi('EXTERNAL',StructOUT.DicomHeader.RTROIObservationsSequence.(fields{i}).RTROIInterpretedType)
        body_index = i;
    end
end
if(isempty(body_index))
    for i=1:length(StructOUT.Struct) % look for exact contour names for patient/body/external
        if(strcmpi(StructOUT.Struct(i).Name,'external') || strcmpi(StructOUT.Struct(i).Name,'body') || strcmpi(StructOUT.Struct(i).Name,'patient') )
            body_index = i;
            break
        end
    end
end
if(not(isempty(body_index)))
    handles = Import_contour(FileNameIN,body_index,'CT',1,handles,{'body'});
else
    disp('Could not find external contour in RT struct. Performing automatic body segmentation...')
    handles = Body_segmentation('CT','body',handles);
    handles.images.info{end}.Color = [0;0;255];
end

% define MC input parameters
SimuParam = struct;
SimuParam.Folder = fullfile(handles.dataPath, ['MCsquare_',strrep(strrep(strrep(datestr(now,'yy-mm-dd-HH-MM-SS'),'-','_'),' ','_'),':','_')]);
SimuParam.CT = 'CT';
SimuParam.Plan = 'Plan';
SimuParam.BDL = BDL_filename;
SimuParam.Scanner = Scanner_dirname;
if(not(isempty(body_index)))
    SimuParam.CropBody = 1;
    SimuParam.BodyContour = 'body';
end
SimuParam.DoseName = 'MC_dose';
SimuParam.NumberOfPrimaries = 1e6;
SimuParam.ComputeStat = 1;
SimuParam.StatUncertainty = uncertainty;
SimuParam = Add_Default_MC2_SimuParams(SimuParam);

% copy BDL and Scanner to temp folder
if(not(exist(SimuParam.Folder,'dir')))
    mkdir(SimuParam.Folder)
end
target=fullfile(SimuParam.Folder,'Scanner_MC');
copyfile(SimuParam.Scanner,target);
SimuParam.Scanner=target;
target=fullfile(SimuParam.Folder,'BDL_MC.txt');
copyfile(SimuParam.BDL,target);
SimuParam.BDL=target;

% Performing MC2 simulation
handles = MC2_simulation(handles,SimuParam);

% Extract dose regions
TPS_dose = Get_reggui_data(handles,'TPS_dose');
prescription = round(compute_prctile(TPS_dose(TPS_dose>0),99.9));
handles = ManualThreshold('TPS_dose',[prescription*0.2 Inf],'all_dose',handles);
handles.images.info{end}.Color = [255;0;0];
handles = ManualThreshold('TPS_dose',[prescription*0.95 Inf],'high_dose_init',handles);
handles = Closure('high_dose_init',[2 2 2],'high_dose',handles);
handles.images.info{end}.Color = [192;192;0];

% Crop high MC dose values in low-density regions
[MC_dose,MC_info] = Get_reggui_data(handles,'MC_dose','images');
CT = Get_reggui_data(handles,'CT','images');
MC_dose(MC_dose>2*prescription & CT<-900)=2*prescription;
handles = Set_reggui_data(handles,'MC_dose',MC_dose,MC_info,'images',1);

% Analyze results
handles = Difference('MC_dose','TPS_dose','dose_diff',handles);
[handles,~,passing_rate] = Gamma_index('TPS_dose','MC_dose','all_dose','gamma',handles,[3,3,0]);
handles = DVH_computation(handles,0,{'TPS_dose','MC_dose'},{'high_dose','all_dose','body'});

% Save reggui project
Save_reggui_handles(handles,fullfile(fileparts(output_filename),'reggui_project.mat'));

% Create colormaps
cm_dose = [0,0,0;0,0,0.625;0,0,0.6875;0,0,0.75;0,0,0.8125;0,0,0.875;0,0,0.9375;...
    0,0,1;0,0.0625,1;0,0.125,1;0,0.1875,1;0,0.25,1;0,0.3125,1;0,0.375,1;...
    0,0.4375,1;0,0.5,1;0,0.5625,1;0,0.625,1;0,0.6875,1;0,0.75,1;0,0.8125,1;...
    0,0.875,1;0,0.9375,1;0,1,1;0.0625,1,0.9375;0.125,1,0.875;0.1875,1,0.8125;...
    0.25,1,0.75;0.3125,1,0.6875;0.375,1,0.625;0.4375,1,0.5625;0.5,1,0.5;...
    0.5625,1,0.4375;0.625,1,0.375;0.6875,1,0.3125;0.75,1,0.25;0.8125,1,0.1875;...
    0.8750,1,0.125;0.9375,1,0.0625;1,1,0;1,0.9375,0;1,0.875,0;1,0.8125,0;...
    1,0.75,0;1,0.6875,0;1,0.625,0;1,0.5625,0;1,0.5,0;1,0.4375,0;1,0.375,0;...
    1,0.3125,0;1,0.25,0;1,0.1875,0;1,0.125,0;1,0.0625,0;1,0,0;0.9375,0,0;...
    0.875,0,0;0.8125,0,0;0.75,0,0;0.6875,0,0;0.625,0,0;0.5625,0,0;0.5,0,0];% jet with 0 as lowest value
cm_dose_diff = [0,0,0.5625;0,0,0.625;0,0,0.6875;0,0,0.75;0,0,0.8125;0,0,0.875;0,0,0.9375;...
    0,0,1;0,0.0625,1;0,0.125,1;0,0.1875,1;0,0.25,1;0,0.3125,1;0,0.375,1;...
    0,0.4375,1;0,0.5,1;0,0.5625,1;0,0.625,1;0,0.6875,1;0,0.75,1;0,0.8125,1;...
    0,0.875,1;0,0.9375,1;0,1,1;0.0625,1,0.9375;0.125,1,0.875;0.1875,1,0.8125;...
    0.25,1,0.75;0.3125,1,0.6875;0.375,1,0.625;0,0,0;0,0,0;...
    0,0,0;0,0,0;0.6875,1,0.3125;0.75,1,0.25;0.8125,1,0.1875;...
    0.8750,1,0.125;0.9375,1,0.0625;1,1,0;1,0.9375,0;1,0.875,0;1,0.8125,0;...
    1,0.75,0;1,0.6875,0;1,0.625,0;1,0.5625,0;1,0.5,0;1,0.4375,0;1,0.375,0;...
    1,0.3125,0;1,0.25,0;1,0.1875,0;1,0.125,0;1,0.0625,0;1,0,0;0.9375,0,0;...
    0.875,0,0;0.8125,0,0;0.75,0,0;0.6875,0,0;0.625,0,0;0.5625,0,0;0.5,0,0];% jet with 0 as central
blue   = [0, 0, 1];
white  = [1, 1, 0.98];
red    = [1, 0, 0];
length_white = 0;
length_blue  = 32;
length_red   = 32;
blue_gradient = [linspace(blue(1),white(1),length_blue)', linspace(blue(2),white(2),length_blue)', linspace(blue(3),white(3),length_blue)'];
red_gradient = [linspace(white(1),red(1),length_red)', linspace(white(2),red(2),length_red)', linspace(white(3),red(3),length_red)'];
cm_gamma(1:length_blue,:,:)=blue_gradient(1:length_blue,:,:);
cm_gamma((length_blue+length_white):(length_blue+length_white+length_red-1),:,:)=red_gradient(1:length_red,:,:);
cm_gamma(1,:) = 0;

% Load colorbars
icons = load('PNG_icons.mat');
cb_dose = icons.cb_dose;
cb_gamma = icons.cb_gamma;

% Generate illustrations
handles = Resample_all(handles,'body',[0],[],'from_mask');
handles = Center_on_plan_isocenter(handles,'Plan');
ct_scale = [-800,1000];
dose_scale = [prescription/5,prescription];
dose_diff_scale = [-prescription/5,prescription/5];
gamma_scale = [0,2];
handles.rendering_frames = {};
handles = Addframe(1,'YX',handles.view_point(3),'CT',handles,{'high_dose','all_dose','body'},'TPS_dose',[],{'ImageScale',ct_scale,'FusionAlpha',0.5,'FusionScale',dose_scale,'FusionColormap',cm_dose});
handles = Addframe(2,'ZX',handles.view_point(2),'CT',handles,{'high_dose','all_dose','body'},'TPS_dose',[],{'ImageScale',ct_scale,'FusionAlpha',0.5,'FusionScale',dose_scale,'FusionColormap',cm_dose});
handles = Addframe(3,'ZY',handles.view_point(1),'CT',handles,{'high_dose','all_dose','body'},'TPS_dose',[],{'ImageScale',ct_scale,'FusionAlpha',0.5,'FusionScale',dose_scale,'FusionColormap',cm_dose});
handles = Addframe(4,'YX',handles.view_point(3),'CT',handles,{'high_dose','all_dose','body'},'MC_dose',[],{'ImageScale',ct_scale,'FusionAlpha',0.5,'FusionScale',dose_scale,'FusionColormap',cm_dose});
handles = Addframe(5,'ZX',handles.view_point(2),'CT',handles,{'high_dose','all_dose','body'},'MC_dose',[],{'ImageScale',ct_scale,'FusionAlpha',0.5,'FusionScale',dose_scale,'FusionColormap',cm_dose});
handles = Addframe(6,'ZY',handles.view_point(1),'CT',handles,{'high_dose','all_dose','body'},'MC_dose',[],{'ImageScale',ct_scale,'FusionAlpha',0.5,'FusionScale',dose_scale,'FusionColormap',cm_dose});
handles = Addframe(7,'YX',handles.view_point(3),'CT',handles,{'high_dose','all_dose','body'},'dose_diff',[],{'ImageScale',ct_scale,'FusionAlpha',0.5,'FusionScale',dose_diff_scale,'FusionColormap',cm_dose_diff});
handles = Addframe(8,'ZX',handles.view_point(2),'CT',handles,{'high_dose','all_dose','body'},'dose_diff',[],{'ImageScale',ct_scale,'FusionAlpha',0.5,'FusionScale',dose_diff_scale,'FusionColormap',cm_dose_diff});
handles = Addframe(9,'ZY',handles.view_point(1),'CT',handles,{'high_dose','all_dose','body'},'dose_diff',[],{'ImageScale',ct_scale,'FusionAlpha',0.5,'FusionScale',dose_diff_scale,'FusionColormap',cm_dose_diff});
handles = Addframe(10,'YX',handles.view_point(3),'CT',handles,{'high_dose','all_dose','body'},'gamma',[],{'ImageScale',ct_scale,'FusionAlpha',0.5,'FusionScale',gamma_scale,'FusionColormap',cm_gamma});
handles = Addframe(11,'ZX',handles.view_point(2),'CT',handles,{'high_dose','all_dose','body'},'gamma',[],{'ImageScale',ct_scale,'FusionAlpha',0.5,'FusionScale',gamma_scale,'FusionColormap',cm_gamma});
handles = Addframe(12,'ZY',handles.view_point(1),'CT',handles,{'high_dose','all_dose','body'},'gamma',[],{'ImageScale',ct_scale,'FusionAlpha',0.5,'FusionScale',gamma_scale,'FusionColormap',cm_gamma});
for i=1:length(handles.rendering_frames)
    image_rendering(handles.rendering_frames{i},[strrep(output_filename,'.png',''),num2str(i)]);
end

figure
plot_dose_volume_histogram(handles.dvhs,ones(length(handles.dvhs),1),[0 100]);
xlabel('Dose [Gy]')
ylabel('Volume [%]')
set(gca,'xtick',[0:5:100])
set(gca,'ytick',[0:5:100])
grid on
leg = findobj(gcf, 'Type', 'Legend');
set(leg,'Location','NorthEast');
set(leg,'FontSize',7);
saveas(gcf,[strrep(output_filename,'.png',''),'_dvh.png'])

% Concatenate all illustrations into one
for i=1:12
    eval(['im',num2str(i),'=imread(''',strrep(output_filename,'.png',''),num2str(i),'_1.png'');']);
end
max_height = max([size(im1,1),size(im2,1),size(im3,1)]);
for i=1:12
    eval(['im',num2str(i),'=imresize(im',num2str(i),',[',num2str(max_height),',NaN]);']);
end
im_dvh = imread([strrep(output_filename,'.png',''),'_dvh.png']);
im_dvh = imresize(im_dvh,[4*size(im1,1),NaN]);
im_tot = [[im1,im2,im3,imresize(cb_dose,[size(im1,1),NaN]);im4,im5,im6,imresize(cb_dose,[size(im1,1),NaN]);im7,im8,im9,imresize(cb_dose,[size(im1,1),NaN]);im10,im11,im12,imresize(cb_gamma,[size(im1,1),NaN])],im_dvh];
imshow(im_tot,'Border','tight')
text(4,size(im1,1)*0.05,'TPS dose','Color','w','FontSize',8);
text(size(im1,2)+size(im2,2)+size(im3,2)+4,size(im1,1)*0.05,num2str(prescription),'Color','w','FontSize',8);
text(size(im1,2)+size(im2,2)+size(im3,2)+8,size(im1,1)*0.85,num2str(0),'Color','w','FontSize',8);
text(4,size(im1,1)*0.05+size(im1,1),'MC dose','Color','w','FontSize',8);
text(size(im1,2)+size(im2,2)+size(im3,2)+4,size(im1,1)*0.05+size(im1,1),num2str(prescription),'Color','w','FontSize',8);
text(size(im1,2)+size(im2,2)+size(im3,2)+8,size(im1,1)*0.85+size(im1,1),num2str(0),'Color','w','FontSize',8);
text(4,size(im1,1)*0.05+2*size(im1,1),'Dose difference','Color','w','FontSize',8);
text(size(im1,2)+size(im2,2)+size(im3,2)+4,size(im1,1)*0.05+2*size(im1,1),num2str(round(prescription/5)),'Color','w','FontSize',8);
text(size(im1,2)+size(im2,2)+size(im3,2)+4,size(im1,1)*0.85+2*size(im1,1),num2str(-round(prescription/5)),'Color','w','FontSize',8);
text(4,size(im1,1)*0.05+3*size(im1,1),'3D Gamma','Color','w','FontSize',8);
text(size(im1,2)+size(im2,2)+size(im3,2)+8,size(im1,1)*0.05+3*size(im1,1),num2str(2),'Color','w','FontSize',8);
text(size(im1,2)+size(im2,2)+size(im3,2)+8,size(im1,1)*0.85+3*size(im1,1),num2str(0),'Color','w','FontSize',8);
text(size(im1,2)+size(im2,2)+size(im3,2)+size(im_dvh,2)*1/3,size(im1,1)*0.15,['3D gamma (3%-3mm) passing rate : ',num2str(passing_rate)],'Color','k','FontSize',14);
fig = gcf;
fig.Color = 'white';
fig.InvertHardcopy = 'off';
saveas(fig,[strrep(output_filename,'.png',''),'.png'])

if(not(isempty(app)))
    close(fig)
    im = imread([strrep(output_filename,'.png',''),'.png']);
    imshow(im,'Parent',app.UIAxesValidation,'Border','tight');
end
