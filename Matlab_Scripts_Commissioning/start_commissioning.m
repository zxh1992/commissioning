%% start_commissioning
% Launches a MC2 commissioning
% 
% Input parametres must include:
% - params.debug
% - params.BDLname
% - params.energies
% - params.input_spot_profile;
% - params.input_pristine_BP;
% - params.input_absolute_dose;
% - params.nozzle2iso
% - params.sadx
% - params.sady
% - params.spacing_ES
% - params.spacing_AD
% - params.RBE_factor
% - params.detector.Shape
% - params.detector.SensitiveVolumeSize
% - params.detector.SensitiveVolumeDepth

function start_commissioning(params,app)

format long e
t_start = clock;

% Chek input
if(ischar(params))
    if(isfile(params)) % params is provided as a file
        try
            load(params);
        catch
            disp('params could not be loaded.')
            return
        end
    elseif(isfolder(params)) % the directory containing the params is provided
        try
            load(fullfile(params,'params'));
        catch
            disp('params not found.')
            return
        end
    end
end

% Running mode
global use_gui;
global use_debug;
use_gui = not(params.remote); % display figures (1 or 0)
use_debug = params.debug; % for debugging (1 or 0)

% Inputs
if(nargin<2)
    bar1 = [];
    bar2 = [];
    bar3 = [];
elseif(use_gui)
    % set progress bars
    app.Panel.AutoResizeChildren = 'off';
    ax = subplot(3,1,1,'Parent',app.Panel);
    disableDefaultInteractivity(ax);
    set(ax.Toolbar,'Visible','off');
    ax.Position=[0 0 1 1];
    ax.XTick=[];
    ax.YTick=[];
    ax.Color=[0.9375 0.9375 0.9375];
    ax.XColor=[0.9375 0.9375 0.9375];
  	ax.YColor=[0.9375 0.9375 0.9375];
    ax.XAxis.Limits=[0,1];
    ax.YAxis.Limits=[0,3];
    bar3 = patch(ax,[0 0 0 0],[0 0 0.8 0.8],[0.3 0.75 0.9375]);
    bar2 = patch(ax,[0 0 0 0],[0 0 0.8 0.8]+1,[0.3 0.75 0.9375]);
    bar1 = patch(ax,[0 0 0 0],[0 0 0.8 0.8]+2,[0.3 0.75 0.9375]);    
end

% Path
code_dir = fileparts(mfilename('fullpath'));
MCsquareDir = fullfile(fileparts(code_dir),'MCsquare'); % path to MCsquare
CT = fullfile(code_dir,'CT','CT.mhd'); % CT file, can be mhd or the first slice of dcm sequence; if mhd, the raw file must be in the same folder
addpath(code_dir, fullfile(code_dir,'MC2Interface')); % path to MCsquare/MC2interface

% Set permissions
if(isunix)
    system(['chmod +x ',fullfile(MCsquareDir,'MCsquare_double')]);
    system(['chmod +x ',fullfile(MCsquareDir,'MCsquare_linux_double')]);
end

%% BDL creation
if(exist(params.BDLname,'file'))
    if(params.update_existing)
        disp('BLD already exists. Backup and update.')
        try
            copyfile(params.BDLname,strrep(params.BDLname,'.txt','_bkp.txt'));
        catch
            disp('Could not copy existing file.')
        end
    else
        disp('BLD already exists. Backup and overwrite.')
        try
            movefile(params.BDLname,strrep(params.BDLname,'.txt','_bkp.txt'));
        catch
            disp('Could not modify/move existing file.')
        end
    end
end
if(not(exist(params.BDLname,'file')))
    disp(['Create BDL template: ' params.BDLname])
    create_BDL(params.BDLname, params.energies, params.nozzle2iso, params.sadx, params.sady, params.input_absolute_dose);
end

%% Phase space tuning
disp('Optimization of phase space parameters')
tune_PS(params.BDLname, params.nozzle2iso, params.input_spot_profile, params.flag_DG, bar1);

%% Energy spectrum tuning
disp('Optimization of energy spectrum parameters')
tune_ES(MCsquareDir, params.BDLname, CT, params.input_pristine_BP, params.spacing_ES, params.debug, params.isocenter_depth_ES, params.integration_diameter_ES, params.nozzle2iso, bar2);

%% Protons per MU tuning
disp('Optimization of protons per MU')
tune_AD(MCsquareDir, params.BDLname, CT, params.input_absolute_dose, params.spacing_AD, params.detector, params.debug, params.sadx, params.sady, params.RBE_factor, params.calibration_depth_AD, params.isocenter_depth_AD, params.field_size_AD, params.spots_spacing_AD, params.SAD_factor_AD, params.IC_correction_factor_AD, params.nozzle2iso, bar3);

disp('Commissioning complete.')
disp(['Processing time = ',num2str(etime(clock,t_start)/60/60),' h'])

