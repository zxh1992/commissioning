function [ CT_info, CT_data ] = openreggui_DicomCT_to_MHD( SourceFile, DestinationFile )

  [CurrentPath,~,~] = fileparts(mfilename('fullpath'));
  addpath([CurrentPath '/lib']);
  
  % Read Dicom CT
  disp(['Read Dicom CT: ' SourceFile]);
  [CT_data,CT_info] = openreggui_Load_3D_DICOM(SourceFile);
  
  CT_info.Size = size(CT_data);

  if(CT_info.Size(1) ~= CT_info.Size(2))
    error('Error during CT conversion: different number of voxels in X and Y directions is not supported');
  end
  
  % Convert data for compatibility with MCsquare
  % These transformations may be modified in a future version
  CT_data = flipdim(CT_data, 1);
  CT_data = flipdim(CT_data, 2);
  
  if(isempty(DestinationFile) == 0)
    % Parse file name
    [DestPath,DestName,DestExt] = fileparts(DestinationFile);
    if(strcmp(DestExt, '.mhd') || strcmp(DestExt, '.MHD'))
      MHD_File = [DestName DestExt];
      RAW_File = [DestName '.raw'];
    else
      MHD_File = [DestName DestExt '.mhd'];
      RAW_File = [DestName DestExt '.raw'];
    end
      
    % Write header file (info)
    disp(['Write MHD CT: ' DestinationFile]);
    fid = fopen([DestPath '/' MHD_File], 'w', 'l');
    fprintf(fid, 'ObjectType = Image\n');
    fprintf(fid, 'NDims = 3\n');
    fprintf(fid, 'DimSize = %d %d %d\n', size(CT_data));
    fprintf(fid, 'ElementSpacing = %f %f %f\n', CT_info.Spacing);
    fprintf(fid, 'Offset = %f %f %f\n', CT_info.ImagePositionPatient);
    fprintf(fid, 'ElementType = MET_FLOAT\n');
    fprintf(fid, 'ElementByteOrderMSB = False\n');
    fprintf(fid, 'ElementDataFile = %s\n', RAW_File);
    fclose(fid);

    % Write binary file (data)
    fid = fopen([DestPath '/' RAW_File], 'w', 'l');
    fwrite(fid, CT_data, 'float', 0, 'l');
    fclose(fid);
  end

end

