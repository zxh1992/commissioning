function BDL = load_BDL(BDL_file)

fid = fopen(BDL_file);
tags = {};

while 1
    tline = fgetl(fid);
    if ~ischar(tline), break, end
    if(not(isempty(strfind(tline,'NominalEnergy'))))
        tags = get_values(tline,0);
        for i=1:length(tags)
            BDL.(tags{i}) = [];
        end
    elseif(not(isempty(tags)))
        values = get_values(tline,1);
        for i=1:length(tags)
            BDL.(tags{i})(end+1) = values(i);
        end
    end
end

fclose(fid);

end

function values = get_values(tline,num)
tline = regexprep(tline,'\t',' ');
for i=1:20
    tline = strrep(tline,'  ',' ');
end
temp = strsplit(tline,' ');
if(num)
    for j=1:length(temp)
        values(j) = str2double(temp{j});
    end
else
    values = temp;
end
end
