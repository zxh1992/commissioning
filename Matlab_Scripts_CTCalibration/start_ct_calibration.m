%% start_ct_calibration
% Launches a CT calibration
% 
% Input parametres must include:
% - params.input_HU
% - params.input_composition
% - params.input_densities
% - params.Scanner_output_name

function start_ct_calibration(params,app)

format long e

% Chek input
if(ischar(params))
    if(isfile(params)) % params is provided as a file
        try
            load(params);
        catch
            disp('params could not be loaded.')
            return
        end
    elseif(isfolder(params)) % the directory containing the params is provided
        try
            load(fullfile(params,'params'));
        catch
            disp('params not found.')
            return
        end
    end
end

% Result figure
if(nargin<2)
    fig = figure;
    for i=1:8
        eval(['ax',num2str(i),' = subplot(2,4,',num2str(i),',''Parent'',fig);']);
    end
else
    app.ResultsPanel.AutoResizeChildren = 'off';
    for i=1:8
        eval(['ax',num2str(i),' = subplot(2,4,',num2str(i),',''Parent'',app.ResultsPanel);']);
        eval(['hold(ax',num2str(i),',''off'');']);
    end
end

% Path
code_dir = fileparts(mfilename('fullpath'));
MCsquareDir = fullfile(fileparts(code_dir),'MCsquare'); % path to MCsquare
DataDir = fullfile(code_dir, '..', 'Data', 'CT_Calibration'); % path to folder with all files needed (phantom and ICRU tissues properties)


%% Schneider 1996 : calibration of coefficients K

% Load phantom data
HU = load(params.input_HU);
HU = HU(:);
densities = load(params.input_densities);
densities = densities(:);
w = load(params.input_composition);
Z = w(1,:);
A = w(2,:);
w = w(3:end,:);
w = w./sum(w,2);

% Constants
rho_w = 1; % water density
avo = 6.02214076*1e23; % avogadro number
w_w = zeros(size(Z));
w_w(Z==1) = 0.1119;
w_w(Z==8) = 0.8881; % water compo

% Optimization of coefficients K
fct = @(x) fun_CT(x, Z, A, w, w_w, rho_w, HU, densities);
x0 = [0.5 0.0004285 0.00001227];
[sol,fval] = fmincon(fct, x0);

% Computation of relative attenuation coefficients and corresponding HU for the found coefficients K
mu_w = sum(w_w./A.*Z.*(sol(1) + Z.^1.86*sol(2) + Z.^3.62*sol(3)));
mu1 = (Z./A.*(sol(1) + Z.^1.86*sol(2) + Z.^3.62*sol(3)))*w';
mu_calc = densities/rho_w.*mu1(:)/mu_w; % mu relative to water
hu_calc = (mu_calc-1)*1000;

% Plot calculated HU vs initial HU obtained with CT
xx=min(hu_calc)-50:0.01:max(hu_calc)+50;
plot(ax1,HU, hu_calc, 'x',xx,xx);
xlabel(ax1,'Measured HU');
ylabel(ax1,'Calculated HU');
title(ax1, 'Calibration');


%% Read Schneider 2000 materials

% Read names and elemental compositions
SchneiderNames = importdata(fullfile(DataDir, 'Names_TissuesSchneider.txt'), '\n');
Compo = load(fullfile(DataDir, 'Compo_TissuesSchneider.txt'))/100;

% Find materials ID in list.dat
list = textread(fullfile(MCsquareDir, '/Materials/list.dat'), '%s','delimiter','\n');
for i=1:length(SchneiderNames)
    mask = find(~cellfun(@isempty,strfind(list(1:length(list)),SchneiderNames{i})));
    mats = strsplit(string(list(mask(1))),' ');
    mat_nb(i,1) = str2double(mats(1));
end


%% Calibration of Schneider 2000 for soft tissues, using previously found K

clearvars W rho ww;

% Data
Z = [1 6 7 8 11 12 15 16 17 19 20 26];
A = [1.008 12.011 14.007 15.999 22.99 24.305 30.974 32.060 35.453 39.098 40.078 55.845];
w_w = [0.1119 0 0 0.8881 0 0 0 0 0 0 0 0];

% Read info on tissues
W = load(fullfile(DataDir,'CompoWW_SoftTissues.txt'))/100;
rho = load(fullfile(DataDir, 'MassDensityWW_SoftTissues.txt'))/1000;
Names_ST = textread(fullfile(DataDir, 'NamesWW_SoftTissues.txt'), '%s', 'delimiter', '\n');

% Add air in data
W(end+1,:) = [0 0.012561 76.508170 23.479269 0 0 0 0 0 0 0 0]/100;
rho(end+1,1) = 0.00121;
W_ST_exact = W;
Names_ST{end+1} = 'air';

% Indices of needed tissues
Adipose3 = 3;
AdrenalGland = 4;
SmallIntestine = 15;
ConnectiveTissue = 12;
Lung = 28;

% HU of materials needed for interpolation
mu_w = rho_w*avo*sum(w_w./A.*(Z*sol(1) + sol(2)*Z.^2.86 + Z.^4.62*sol(3)));
HU_SoftTissues(size(W,1),1) = 1000*(rho(end)*avo*sum(W(end,:)./A.*(Z*sol(1) + sol(2)*Z.^2.86 + Z.^4.62*sol(3)))/mu_w - 1);
HU_SoftTissues(Adipose3,1) = 1000*(rho(Adipose3)*avo*sum(W(Adipose3,:)./A.*(Z*sol(1) + sol(2)*Z.^2.86 + Z.^4.62*sol(3)))/mu_w - 1);
HU_SoftTissues(AdrenalGland,1) = 1000*(rho(AdrenalGland)*avo*sum(W(AdrenalGland,:)./A.*(Z*sol(1) + sol(2)*Z.^2.86 + Z.^4.62*sol(3)))/mu_w - 1);
HU_SoftTissues(SmallIntestine,1) = 1000*(rho(SmallIntestine)*avo*sum(W(SmallIntestine,:)./A.*(Z*sol(1) + sol(2)*Z.^2.86 + Z.^4.62*sol(3)))/mu_w - 1);
HU_SoftTissues(ConnectiveTissue,1) = 1000*(rho(ConnectiveTissue)*avo*sum(W(ConnectiveTissue,:)./A.*(Z*sol(1) + sol(2)*Z.^2.86 + Z.^4.62*sol(3)))/mu_w - 1);

% Calibration
mu_w = rho_w*avo*sum(w_w./A.*(Z*sol(1) + sol(2)*Z.^2.86 + Z.^4.62*sol(3)));
for i=1:size(W,1)
    mu_reftissues(i,1) = rho(i)*avo*sum(W(i,:)./A.*(Z*sol(1) + sol(2)*Z.^2.86 + Z.^4.62*sol(3)));
    HU_SoftTissues(i,1) = 1000*(mu_reftissues(i)/mu_w-1);
    if (HU_SoftTissues(i,1) <= HU_SoftTissues(AdrenalGland,1) && HU_SoftTissues(i,1) >= HU_SoftTissues(Adipose3))
        Density_SoftTissues(i,1) = (rho(Adipose3)*HU_SoftTissues(AdrenalGland) - rho(AdrenalGland)*HU_SoftTissues(Adipose3) + (rho(AdrenalGland) - rho(Adipose3))*HU_SoftTissues(i))/(HU_SoftTissues(AdrenalGland) - HU_SoftTissues(Adipose3));
        W_SoftTissues(i,:) = rho(Adipose3)*(HU_SoftTissues(AdrenalGland) - HU_SoftTissues(i)) / (rho(Adipose3)*HU_SoftTissues(AdrenalGland) - rho(AdrenalGland)*HU_SoftTissues(Adipose3) + (rho(AdrenalGland)-rho(Adipose3))*HU_SoftTissues(i)) * (W(Adipose3,:) - W(AdrenalGland,:)) + W(AdrenalGland,:);
    elseif (HU_SoftTissues(i,1) <= HU_SoftTissues(ConnectiveTissue,1) && HU_SoftTissues(i,1) >= HU_SoftTissues(SmallIntestine,1))
        Density_SoftTissues(i,1) = (rho(SmallIntestine)*HU_SoftTissues(ConnectiveTissue) - rho(ConnectiveTissue)*HU_SoftTissues(SmallIntestine) + (rho(ConnectiveTissue) - rho(SmallIntestine))*HU_SoftTissues(i))/(HU_SoftTissues(ConnectiveTissue) - HU_SoftTissues(SmallIntestine));
        W_SoftTissues(i,:) = rho(SmallIntestine)*(HU_SoftTissues(ConnectiveTissue) - HU_SoftTissues(i)) / (rho(SmallIntestine)*HU_SoftTissues(ConnectiveTissue) - rho(ConnectiveTissue)*HU_SoftTissues(SmallIntestine) + (rho(ConnectiveTissue)-rho(SmallIntestine))*HU_SoftTissues(i)) * (W(SmallIntestine,:) - W(ConnectiveTissue,:)) + W(ConnectiveTissue,:);
    elseif (HU_SoftTissues(i,1) < HU_SoftTissues(Adipose3,1) && HU_SoftTissues(i,1) >= HU_SoftTissues(end,1))
        Density_SoftTissues(i,1) = interp1([HU_SoftTissues(end,1) HU_SoftTissues(Adipose3,1)], [rho(end) rho(Adipose3)], HU_SoftTissues(i,1));
        W_SoftTissues(i,:) = W(i,:);
    elseif (HU_SoftTissues(i,1) < (HU_SoftTissues(AdrenalGland)+HU_SoftTissues(SmallIntestine))/2)
        W_SoftTissues(i,:) = W(AdrenalGland,:);
        Density_SoftTissues(i,1) = interp1([HU_SoftTissues(AdrenalGland,1) HU_SoftTissues(SmallIntestine,1)], [rho(AdrenalGland) rho(SmallIntestine)], HU_SoftTissues(i,1));  
    elseif (HU_SoftTissues(i,1) >= (HU_SoftTissues(AdrenalGland)+HU_SoftTissues(SmallIntestine))/2)
        W_SoftTissues(i,:) = W(SmallIntestine,:);
        Density_SoftTissues(i,1) = interp1([HU_SoftTissues(AdrenalGland,1) HU_SoftTissues(SmallIntestine,1)], [rho(AdrenalGland) rho(SmallIntestine)], HU_SoftTissues(i,1));
    end
end

% Schneider materials
HU_Schneider = zeros(24,1);
HU_Schneider(1) = HU_SoftTissues(end);
HU_Schneider(2) = HU_SoftTissues(Lung);
HU_Schneider(3) = HU_SoftTissues(Adipose3);
k = 1:4;
rho1 = rho(Adipose3);
rho2 = rho(AdrenalGland);
w1 = W(Adipose3,k);
w2 = W(AdrenalGland,k);
H1 = HU_SoftTissues(Adipose3);
H2 = HU_SoftTissues(AdrenalGland);
for i=4:6
    HU_Sch = ((rho1*H2-rho2*H1)*(Compo(i,k)-w2)./(w1 - w2) - rho1*H2) ./ (-rho1 - ((rho2-rho1)*(Compo(i,k)-w2)./(w1 - w2)));
    HU_Schneider(i) = mean(HU_Sch);
end
HU_Schneider(7) = HU_SoftTissues(AdrenalGland);
HU_Schneider(8) = mean(HU_SoftTissues(HU_SoftTissues < HU_SoftTissues(ConnectiveTissue) & HU_SoftTissues >= HU_SoftTissues(SmallIntestine)));
HU_Schneider(9) = HU_SoftTissues(ConnectiveTissue);


%% Calibration of Schneider 2000 for bones, using previously found K

% Data
Z = [1 6 7 8 11 12 15 16 17 19 20 26];
A = [1.008 12.011 14.007 15.999 22.99 24.305 30.974 32.060 35.453 39.098 40.078 55.845];
w_w = [0.1119 0 0 0.8881 0 0 0 0 0 0 0 0];

% Read info on tissues
W = load(fullfile(DataDir, 'CompoWW_SkeletalTissues.txt'))/100;
rho = load(fullfile(DataDir, 'MassDensityWW_SkeletalTissues.txt'))/1000;
Names_B = textread(fullfile(DataDir, 'NamesWW_SkeletalTissues.txt'), '%s', 'delimiter', '\n');

% Indices of needed tissues
CorticalBone = 2;
RMarrow = 3;
YMarrow = 5;

% Add a tissue which is mixture of red and yellow marrows
W(end+1,:) = 0.5*W(YMarrow,:) + 0.5*W(RMarrow,:);
rho(end+1) = 1; % from Schneider 2000
W_B_exact = W;
Names_B{end+1} = 'mixed marrow';

% HU of materials needed for interpolationHU_Schneider(1) = HU_SoftTissues(end);
HU_Schneider(2) = HU_SoftTissues(Lung);
HU_Schneider(3) = HU_SoftTissues(Adipose3);
mu_w = rho_w*avo*sum(w_w./A.*(Z*sol(1) + sol(2)*Z.^2.86 + Z.^4.62*sol(3)));
HU_SkeletalTissues(CorticalBone,1) = 1000*(rho(CorticalBone)*avo*sum(W(CorticalBone,:)./A.*(Z*sol(1) + sol(2)*Z.^2.86 + Z.^4.62*sol(3)))/mu_w - 1);
HU_SkeletalTissues(length(rho),1) = 1000*(rho(end)*avo*sum(W(end,:)./A.*(Z*sol(1) + sol(2)*Z.^2.86 + Z.^4.62*sol(3)))/mu_w - 1);

% Calibration
for i=1:size(W,1)
    mu_reftissues(i,1) = rho(i)*avo*sum(W(i,:)./A.*(Z*sol(1) + sol(2)*Z.^2.86 + Z.^4.62*sol(3)));
    HU_SkeletalTissues(i,1) = 1000*(mu_reftissues(i)/mu_w-1); % HU_SkeletalTissues(i,1) = 1000*(mu_reftissues(i)/mu_w-1) !!!!!!!!!!!!!!!!!!!!!!!!
    Density_SkeletalTissues(i,1) = (rho(end)*HU_SkeletalTissues(CorticalBone) - rho(CorticalBone)*HU_SkeletalTissues(end) + (rho(CorticalBone) - rho(end))*HU_SkeletalTissues(i))/(HU_SkeletalTissues(CorticalBone) - HU_SkeletalTissues(end));
    W_SkeletalTissues(i,:) = rho(end)*(HU_SkeletalTissues(CorticalBone) - HU_SkeletalTissues(i)) / (rho(end)*HU_SkeletalTissues(CorticalBone) - rho(CorticalBone)*HU_SkeletalTissues(end) + (rho(CorticalBone)-rho(end))*HU_SkeletalTissues(i)) * (W(end,:) - W(CorticalBone,:)) + W(CorticalBone,:);
    W_check(i,:) = (1524-HU_SkeletalTissues(i,1))/(1566+0.92*HU_SkeletalTissues(i,1))*(W(end,:) - W(CorticalBone,:)) + W(CorticalBone,:);
end

% Schneider materials
k = [1:4 7 11];
rho1 = rho(end);
rho2 = rho(CorticalBone);
w1 = W(end,k);
w2 = W(CorticalBone,k);
H1 = HU_SkeletalTissues(end);
H2 = HU_SkeletalTissues(CorticalBone);
for i=10:23
    HU_Sch = ((rho1*H2-rho2*H1)*(Compo(i,k)-w2)./(w1 - w2) - rho1*H2) ./ (-rho1 - ((rho2-rho1)*(Compo(i,k)-w2)./(w1 - w2)));
    HU_Schneider(i) = mean(HU_Sch);
end
HU_Schneider(24) = HU_SkeletalTissues(CorticalBone);


%% Density calibration curve

clearvars HU;
HU = [HU_SoftTissues(end)-100; HU_SoftTissues(end); HU_SoftTissues(Adipose3); HU_SoftTissues(AdrenalGland); HU_SoftTissues(SmallIntestine); HU_SoftTissues(ConnectiveTissue); HU_SoftTissues(ConnectiveTissue)+1; HU_SkeletalTissues(CorticalBone)];
D = [Density_SoftTissues(end); Density_SoftTissues(end); Density_SoftTissues(Adipose3); Density_SoftTissues(AdrenalGland); Density_SoftTissues(SmallIntestine); Density_SoftTissues(ConnectiveTissue); interp1([HU_SkeletalTissues(end) HU_SkeletalTissues(CorticalBone)], [Density_SkeletalTissues(end) Density_SkeletalTissues(CorticalBone)], HU_SoftTissues(ConnectiveTissue)+1); Density_SkeletalTissues(CorticalBone)];
HU = round(HU);

% Write CC
mkdir(params.Scanner_output_name);
fid = fopen(fullfile(params.Scanner_output_name, 'HU_Density_Conversion.txt'),'w');
fprintf(fid, '%s\n', '# ===================');
fprintf(fid, '%s\n', '# HU	density g/cm3');
fprintf(fid, '%s\n', '# ===================');
fprintf(fid, '%d\t%1.6f\n', [HU D]');
fclose(fid);

% Graphical check
plot(ax5, [HU_SoftTissues; HU_SkeletalTissues], [Density_SoftTissues; Density_SkeletalTissues], '*', HU, D, '.-')
xlabel(ax5,'HU');
ylabel(ax5,'Mass density');
legend(ax5, 'Tissues from ICRU database (interpolated)', 'Prediction of CT calibration');
title(ax5, 'HU->Density curve');


%% Material calibration curve

% Delete skeletal tissues having HU smaller than connective tissue
ind = find(HU_SkeletalTissues <= HU_SoftTissues(ConnectiveTissue)); % remove bone/marrow tissues with HU below Connective tissue
HU_SkeletalTissues(ind) = [];
W_SkeletalTissues(ind,:) = [];
Density_SkeletalTissues(ind) = [];
W_B_exact(ind,:) = [];
Names_B(ind) = [];

% Group all calculated HU of ICRU materials
HU_All = [HU_SoftTissues; HU_SkeletalTissues];
W_All = [W_SoftTissues; W_SkeletalTissues];
Density_All = [Density_SoftTissues; Density_SkeletalTissues];
W_All_exact = [W_ST_exact; W_B_exact];
Names_All = [Names_ST; Names_B];
[HU_All,ind] = sort(HU_All);
W_All = W_All(ind,:);
Density_All = Density_All(ind);
W_All_exact = W_All_exact(ind,:);
Names_All = Names_All(ind);

% HU bins + correction for some specific tissues
HU_Bins = round([-1000; (HU_Schneider(2:end)+HU_Schneider(1:end-1))/2]);
HU_Bins(2) = -950;
HU_Bins(3) = round(HU_Schneider(3) - 25);
HU_Bins(8) = round(mean([HU_SoftTissues(AdrenalGland) HU_SoftTissues(SmallIntestine)]));
HU_Bins(10) = round(min([(HU_SoftTissues(ConnectiveTissue) + 25), HU_Schneider(10)]));
HU_Bins(11) = round(mean([HU_Schneider(10) HU_Schneider(11)]));

% Write HU_Material_Conversion
curve = [HU_Bins(:) mat_nb(:)];
curve = sortrows(curve,1);
fid = fopen(fullfile(params.Scanner_output_name, 'HU_Material_Conversion.txt'),'w');
fprintf(fid, '%s\n', '# ===================');
fprintf(fid, '%s\n', '# HU material label');
fprintf(fid, '%s\n', '# ===================');
fprintf(fid, '%d\t%d\n', curve');
fclose(fid);

% Graphical check
v = floor(HU_All(1)):1:floor(HU_All(end));
elements = {'H','C','N','O','Na','Mg','P','S','Cl','K','Ca','Fe'};
for i=1:size(W_All,2)
    CC(:,i) = 100*interp1(HU_Bins, Compo(:,i), v, 'previous', 'extrap');
end
elements_to_display = [1,2,3,4,7,11]; % for hydrogen, carbon, nitrogen, oxygen, phosphorus and calcium
subplot_to_display = [2,3,4,6,7,8];
for index=1:length(elements_to_display)
    i = elements_to_display(index);
    err(:,i) = round((interp1(v, CC(:,i), HU_All, 'previous', 'extrap') - 100*W_All_exact(:,i)), 4);
    eval(['plot(ax',num2str(subplot_to_display(index)),',HU_All, 100*W_All(:,i),''*-'')']);
    eval(['hold(ax',num2str(subplot_to_display(index)),',''on'');']);
    eval(['plot(ax',num2str(subplot_to_display(index)),',v, CC(:,i),''.'',''MarkerSize'', 8)']);
    eval(['plot(ax',num2str(subplot_to_display(index)),',HU_Schneider, 100*Compo(:,i), ''o'', ''MarkerSize'', 10.5)']);
    eval(['xlabel(ax',num2str(subplot_to_display(index)),',''HU'')']);
    eval(['ylabel(ax',num2str(subplot_to_display(index)),',''Elemental fraction (%)'')']);
    eval(['title(ax',num2str(subplot_to_display(index)),',[elements{i},''  (error: '',num2str(round(mean(err(:,i)*10))/10),'' +/- '',num2str(round(std(err(:,i))*10)/10),'')''])']);
end
legend(ax4,'ICRU tissues (interpolated)', 'CT calibration', 'Schneider tissues','Location','NorthEast');

% Write errors next to corresponding tissues names
format short
Title{1,1} = '#######################################################';
Title{2,1} = '# Errors on elemental composition for each tissue (%) #';
Title{3,1} = '#######################################################';
Results = ['TISSUE' elements([1 2 3 4 7 11]); Names_All num2cell(err(:,[1 2 3 4 7 11]))];
fid = fopen('Results_CTcali.txt','w');
fprintf(fid, '%s\n', string(Title));
for i=1:size(Results,1)
    fprintf(fid, '%60s\t\t%s\t%s\t%s\t%s\t%s\t%s\n', string(Results(i,:)));
    fprintf(fid, '\n');
end
fclose(fid);
